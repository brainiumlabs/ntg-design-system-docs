# NTG DESIGN SYSTEM DOCUMENTATION#

This is the documentation webpage for the NTG Design System

## Prerequisites ##

* [NodeJS](https://nodejs.org/en/download/)

## Folder Structure ##

    .
    ├── dist                    # Compiled files
    ├── src                     # Source files
        ├── assets              # Public file assets
        ├── core                # Government Design System components
        ├── ntg                 # Custom scripts and styles
        ├── pages               # HTML pages
    ├── package-lock.json
    ├── package.json
    └── README.md

**Note**: All custom css and js should be done in the `src/ntg` folder. Do not change the files in `src/core` unless you know what you are doing.

## Install ##

Once cloned or downloaded, install the dependencies:

```bash
npm install
```

For development run:

```bash
npm run dev
```

For production builds run:

```bash
npm run prod
```