(function () {
  initResponsiveMenu();
  initMainNavToggles();
  initSideNavToggles();
  initSideNav();
  initMobileNav();
})();

// NTG Responsive Menu
function initResponsiveMenu() {
  responsivemenu.init({
    wrapper: document.querySelector('.au-main-nav__menu-inner')
  });
}

// Appends the toggle buttons to the navigation if nested items are present on initial page load
function initMainNavToggles() {
  var mainNav = document.querySelector('.au-main-nav');
  var nestedItems = mainNav.querySelectorAll('a ~ ul');

  for(var i = 0 ; i < nestedItems.length; i++) {
    nestedItems[i].insertAdjacentHTML('beforebegin', '<a href="#" class="au-main-nav__collapser collapsed"><div class="collapser">Toggle</div></a>');
    nestedItems[i].classList.add('collapse');
  }

}

// Appends the toggle buttons to the navigation if nested items are present on initial page load
function initSideNavToggles() {
  var sideNav = document.querySelector('.ntg-side-nav-accordion');

  // only check for toggles if sidenav toggles are expected to be enabled
  if(!sideNav) {
    return false;
  }

  var nestedItems = sideNav.querySelectorAll('a ~ ul');

  for(var i = 0 ; i < nestedItems.length; i++) {
    nestedItems[i].insertAdjacentHTML('beforebegin', '<a href="#" class="au-side-nav__collapser collapsed"><div class="collapser">Toggle</div></a>');
    nestedItems[i].classList.add('collapse');
  }

}

// Adds accordion functionality to nested items in the side navigation
function initSideNav() {
  var sideNavParents = document.querySelectorAll('.au-side-nav__collapser');

  for (var i = 0; i < sideNavParents.length; i++) {

    sideNavParents[i].addEventListener('click', function (e) {
      e.preventDefault();
      
      var thisNext = this.parentElement.getElementsByClassName('collapse')[0];

      if (thisNext.classList.contains('show')) {
        thisNext.classList.remove('show');
        this.classList.add('collapsed');
      } else {
        thisNext.classList.add('show');
        this.classList.remove('collapsed');
      }
    });

  }

}

// Adds accordion functionality to nested items in the mobile navigation
function initMobileNav() {
  var mainNavParents = document.querySelectorAll('.au-main-nav__collapser');

  for (var i = 0; i < mainNavParents.length; i++) {

    mainNavParents[i].addEventListener('click', function (e) {
      e.preventDefault();
      
      var thisNext = this.parentElement.getElementsByClassName('collapse')[0];

      if (thisNext.classList.contains('show')) {
        thisNext.classList.remove('show');
        this.classList.add('collapsed');
      } else {
        thisNext.classList.add('show');
        this.classList.remove('collapsed');
      }
    });

  }

}